const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './*_test.js',
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'https://github.com/',
      show: true,  //show giao dien cho nguoi dung thay, false: sẽ chạy ngầm, khi chạy trên CI sẽ là false
      windowSize: '1200x900'
    }
  },
  include: {
    I: './steps_file.js',
    LoginPageMobile:"./pages/login_Mobile_Page.js", 
    LoginStepMobile:"./steps/login_Mobile_Step.js"
  },
  bootstrap: null,
  mocha: {},
  name: 'Codecepjs_web',
  plugins: {
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
}