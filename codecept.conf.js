const event = require('codeceptjs').event
exports.config = {
  rerun: {
    // run 6 times until 1st success
    minSuccess: 1,
    maxReruns: 6,
  },
  output: './output',
  helpers: {
    Appium: {
      platform: 'Android',
      device: 'Galaxy A20s',
      automationName: 'Appium',
      desiredCapabilities: {
        appPackage: 'com.leapxpert.manager.qa',// thông tin của app kết nối
        appActivity: 'com.leapxpertapp.MainActivity',//dùng tool để lấy
        noReset: false, // giữ giá trị của lần thao tác trước run
        fullReset: false,// tự search
        automationName: 'UIAutomator2', // test trên thiết bị android, nếu ios giá trị sẽ khác
        newCommandTimeout: 30000, // có những trường hợp k cần put vào, có giá trị default, để gọi sécion
      },
    },

  },
  include: {
    I: './steps_file.js',
    LoginPageMobile:"./pages/login_Mobile_Page.js", 
    LoginStepMobile:"./steps/login_Mobile_Step.js"
  },
  mocha: {
    reporterOptions: {
      mochaFile: 'output/result.xml',
      reportDir: 'output/result.html',
    },
  },
  hooks: [],
  plugins: {
    screenshotOnFail: {
      enabled: true,
    },
    retryFailedStep: {
      enabled: true,
    },
  },
  tests: '*',
  name: 'lxp-web-automation',
}
