const { I, LoginPageMobile } = inject();

module.exports = {
  logIn(qrCode, passWord, optCode)
  {
    // nhan update
    I.waitForElement(LoginPageMobile.login.updatebtn, 30);
    I.tap(LoginPageMobile.login.updatebtn);

    // console.log(qrCode[0])

    //nhap skip
    I.waitForElement(LoginPageMobile.login.skipbtn, 30);
    I.tap(LoginPageMobile.login.skipbtn);
    I.wait(30);

    //nhap qrCode
    I.seeElement(LoginPageMobile.login.code0, 30);
    I.fillField(LoginPageMobile.login.code0, qrCode[0]);
    I.seeElement(LoginPageMobile.login.code1);
    I.fillField(LoginPageMobile.login.code1, qrCode[1]);
    I.seeElement(LoginPageMobile.login.code2);
    I.fillField(LoginPageMobile.login.code2, qrCode[2]);
    I.seeElement(LoginPageMobile.login.code3);
    I.fillField(LoginPageMobile.login.code3, qrCode[3]);
    I.seeElement(LoginPageMobile.login.code4);
    I.fillField(LoginPageMobile.login.code4, qrCode[4]);
    I.seeElement(LoginPageMobile.login.code5);
    I.fillField(LoginPageMobile.login.code5, qrCode[5]);
    I.seeElement(LoginPageMobile.login.code6);
    I.fillField(LoginPageMobile.login.code6, qrCode[6]);
    I.seeElement(LoginPageMobile.login.code7);
    I.fillField(LoginPageMobile.login.code7, qrCode[7]);
    I.seeElement(LoginPageMobile.login.code8);
    I.fillField(LoginPageMobile.login.code8, qrCode[8]);
    I.seeElement(LoginPageMobile.login.code9);
    I.fillField(LoginPageMobile.login.code9, qrCode[9]);
    I.wait(30);


    //nhap password
    I.seeElement(LoginPageMobile.login.passWord);
    I.fillField(LoginPageMobile.login.passWord, passWord)
    I.tap(LoginPageMobile.login.loginbtn)
    I.wait(30);


    //nhap opt
    I.seeElement(LoginPageMobile.login.otp1);
    I.fillField(LoginPageMobile.login.otp1, optCode[0]);
    I.seeElement(LoginPageMobile.login.otp2);
    I.fillField(LoginPageMobile.login.otp2, optCode[1]);
    I.seeElement(LoginPageMobile.login.otp3);
    I.fillField(LoginPageMobile.login.otp3, optCode[2]);
    I.seeElement(LoginPageMobile.login.otp4);
    I.fillField(LoginPageMobile.login.otp4, optCode[3]);
    I.seeElement(LoginPageMobile.login.otp5);
    I.fillField(LoginPageMobile.login.otp5, optCode[4]);
    I.seeElement(LoginPageMobile.login.otp6);
    I.fillField(LoginPageMobile.login.otp6, optCode[5]);
    I.wait(20);

    // confirm login

    I.waitForText(LoginPageMobile.login.userProfile, 30);

  }
  // insert your locators and methods here
}
